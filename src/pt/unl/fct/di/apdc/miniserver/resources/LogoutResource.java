package pt.unl.fct.di.apdc.miniserver.resources;

import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;

@Path("/logout")
public class LogoutResource {
	
	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(RegisterResource.class.getName());
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	
	
	public LogoutResource() {} // Nothing to be done here...
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response doLogoutV1(@QueryParam("tkn") String tokenId) {
		
        Transaction txn = datastore.beginTransaction();
		
		Query ctrQuery = new Query("Token");
		
		List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());
		
		for(Entity r: results){
			if(r.getKey().getName().equals(tokenId)){
				datastore.delete(txn, r.getKey());
				txn.commit();
				return Response.ok("Logged out successfully").build();
			}
		}
		
		txn.rollback();
		return Response.status(Status.BAD_REQUEST).entity("User with the given token is not logged in.").build();
		
	}

}
