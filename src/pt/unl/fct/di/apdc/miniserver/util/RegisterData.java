package pt.unl.fct.di.apdc.miniserver.util;

public class RegisterData {

	public String email;
	public String name;
	public String tlmFixo;
	public String tlmMovel;
	public String street;
	public String complementary;
	public String locality;
	public String postCode;
	public String nif;
	public String cc;
	public String password;
	
	public RegisterData() {
		
	}
	
	public RegisterData(String email, String name, String tlmFixo, String tlmMovel, String street, String complementary, String locality, String postCode, String nif, String cc, String password) {
		
			this.email = email;
			this.name = name;
			this.tlmFixo = tlmFixo;
			this.tlmMovel = tlmMovel;
			this.street = street;
			this.complementary = complementary;
			this.locality = locality;
			this.postCode = postCode;
			this.nif = nif;
			this.cc = cc;
			this.password = password;
	}
	
	private boolean validField(String value) {
		return value != null && !value.equals("");
	}
	
	public boolean validRegistration() {
		return validField(email) &&
			   validField(name) &&
			   validField(tlmFixo)&&
			   validField(tlmMovel)&&
			   validField(street)&&
			   validField(complementary)&&
			   validField(locality)&&
			   validField(postCode)&&
			   validField(nif)&&
			   validField(cc)&&
			   validField(password) &&
			   email.contains("@");		
	}
	
}
