
function logout(){
	var data= localStorage.getItem('tokenID');
	if(data == null){
		alert("Login first please!");
	}
	else{
		 $.ajax({
		        type: "POST",
		        //url: "http://localhost:8888/rest/logout/?tkn="+data,
		        url: "http://miniserver-162416.appspot.com/rest/logout/?tkn="+data,
		        contentType: "application/json; charset=utf-8",
		        crossDomain: true,
		        data: JSON.stringify(data),
		        //dataType: "json",
		        success: function(response) {
		            if(response) {
		                alert("Logout successful!");
		                // Remove token id
		                localStorage.removeItem('tokenID');
		                location.href = '../index.html';
		            }
		            else {
		                alert("No response");
		            }
		        },
		        error: function(response) {
		            alert("Error: "+ response.status);
		        },
		       
		    });
		 event.preventDefault()
	}
	
	
}
