captureData = function(event) {
    var data = $('form[name="register"]').jsonify();
    console.log(data);
    $.ajax({
        type: "POST",
        url: "http://miniserver-162416.appspot.com/rest/register/",
        //url: "http://localhost:8888/rest/register/",
        contentType: "application/json; charset=utf-8",
        crossDomain: true,
        //dataType: "json",
        success: function(response) {
            if(response) {
                alert("Registretion successful!");
                location.href = '../restfrontend.html';
            }
            else {
                alert("No response");
            }
        },
        error: function(response) {
            alert("Error: "+ response.status);
        },
        data: JSON.stringify(data)
    });

    event.preventDefault();
};

window.onload = function() {
    var frms = $('form[name="register"]');     //var frms = document.getElementsByName("login");
    frms[0].onsubmit = captureData;
}
